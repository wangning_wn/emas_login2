#!/bin/bash

# 解压 Prototype 后，cd 到该目录，然后运行 ./Rename.sh XXXX

appname=$1
if [[ -z "$appname" ]]; then
	read -p 'Specify an application bundle name: ' appname
	if [[ -z "$appname" ]]; then
		echo "ERROR: bundle is not valid.";
		exit 1;
	fi
fi

BASE_APPNAME="Prototype"

function proc_file_content() {
	sed "s/$BASE_APPNAME/${appname}/g" "$1" > "$1"'.tmp'
	rm -rf "$1"
	mv "$1"'.tmp' "$1"
}

function replace_filename() {
	fn="$1"
	dirn=$(dirname "$fn")
	fbase=$(basename "$fn")
	replace=`echo "$fbase" | sed "s/$BASE_APPNAME/${appname}/g"`
	target="$dirn/$replace"
	echo $target
	if [ "$replace" != "$fbase" ]; then
		mv "$fn" "$target"
	fi

	if [ -f "$target" ]; then
		proc_file_content "$target"
	fi
}

function list() {
	ls "$1" | while read file
	do
		if [ "$file" != "Rename.sh" ]; then
			if [ -d "$1/$file" ] ; then
				if [[ ${file##*.} == "framework" || ${file##*.} == "xcassets" || ${file##*.} == "bundle" ]]; then
					# 不再深度遍历
					echo "Ignore $1/$file"
				else
					list "$1/$file";
				fi
			fi
			replace_filename "$1/$file"
		fi
	done
}

list "$PWD"